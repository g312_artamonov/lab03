
#include <stdio.h>

int main() 
{
	float x0, a, x, t, v0, rodik ; // input value

	printf("x0: "); scanf("%f", &x0); // scan х0
	printf("v0: "); scanf("%f", &v0); // scan у0
	printf("a: "); scanf("%f", &a); // scan а
	printf("t: "); scanf("%f", &t); // scan t
	
	x = x0 + v0 * t + (a * (t * t))/2; // solving
	
	printf("x = %.3f\n", x); // output values
	
	return 0;
}
