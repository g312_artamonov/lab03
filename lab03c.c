
#include <stdio.h>


	float x0, y0, v0x, v0y, ax, ay, t;
	float x, y, rodik;

int main() 
{

	printf("x0, "); 
	scanf("%f", &x0); 
	printf("y0: "); 
	scanf("%f", &y0); 
	printf("v0x, "); 
	scanf("%f", &v0x);
	printf("v0y: "); 
	scanf("%f", &v0y); 
	printf("ax, "); 
	scanf("%f", &ax); 
	printf("ay: "); 
	scanf("%f", &ay);
	printf("t: "); 
	scanf("%f", &t); 
	
	x = x0 + v0x * t + (ax * (t * t))/2; 
	y = y0 + v0y * t + (ay * (t * t))/2; 

	printf("x = %.3f\n", x); // output values
	printf("y = %.3f\n", y); // output values

	return 0;
}
